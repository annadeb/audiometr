﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Audiometr
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            chart.Series.Clear();
        }
        Sound sn = new Sound(20,3000,10);
        List<ushort> freqlist = new List<ushort>();
        List<double> vollist = new List<double>();
        bool soundheard = false;

        ushort[] tabfreq = new ushort[] {125, 250, 500, 1000, 2000, 3000, 4000, 6000, 8000}; //freq 20, 1500  cancelled
        double multiplier = 1;
        private void buttonPlayClick(object sender, EventArgs e)
        {
            BackgroundWorker bw = new BackgroundWorker();

            // this allows our worker to report progress during work
            bw.WorkerReportsProgress = true;

            // what to do in the background thread
            bw.DoWork += new DoWorkEventHandler(
            delegate (object o, DoWorkEventArgs args)
            {
                BackgroundWorker b = o as BackgroundWorker;
                
                for (ushort i = 0; i < tabfreq.Length; i++) //freq 10... 20K
                {                   
                    sn.Frequency = tabfreq[i];
                    string freq = "freq" + tabfreq[i];
                    b.ReportProgress(tabfreq[i]);
                    multiplier = 1;
                    if (tabfreq[i] == 125)
                    {
                        sn.Volume = 15;
                    }
                    else if (tabfreq[i] >= 125 ) //&& tabfreq[i]< 2000)
                    {
                        sn.Volume = 10;
                    }
                   
                    for (ushort j = sn.Volume; j <= 65535; j += 1)
                    {
                        if (tabfreq[i] == 125)
                        {
                            j += 10;
                            if (j>25)
                            {
                                j -= 1;
                            }
                            if (j >= 40)
                            {
                                multiplier *= 10;
                            }
                        }
                        else if (tabfreq[i] >= 250 && tabfreq[i] < 2000)
                        {
                            j += 5;
                            if (j > 15)
                            {
                                j -= 1;
                            }
                            if (j >= 25)
                            {
                                multiplier *= 10;
                            }
                        }
                        else //freq>=3000
                        {
                            j += 2;
                            if (j > 12)
                            {
                                j -= 1;
                            }
                            if (j >= 18)
                            {
                                multiplier *= 5;
                            }
                        }
                        Thread.Sleep(10);
                        sn.Volume = j;
                        sn.PlaySound();
                        Thread.Sleep(3010);
                        if (sn.Volume > 60000)
                        {
                            break;
                        }

                        Console.WriteLine(j);
                        if (soundheard)
                        {
                            soundheard = false;
                            break;
                        }
                    }
                }

            });

            bw.ProgressChanged += new ProgressChangedEventHandler(
            delegate (object o, ProgressChangedEventArgs args)
            {
                label1.Text = string.Format("Frequency = {0} ", args.ProgressPercentage);
            });

            bw.RunWorkerAsync();
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void chart1_Click(object sender, EventArgs e)
        {
           
        }

        private void buttonSoundHeard(object sender, EventArgs e)
        {
            freqlist.Add(sn.Frequency);
            double logarytm = Math.Log10(sn.Volume * multiplier);
            vollist.Add(logarytm);

            soundheard = true;
        }

        private void buttonPlot(object sender, EventArgs e)
        {
            chart.Series.Clear();

            // Set palette.
            chart.Palette = ChartColorPalette.SeaGreen;

            // Set title.
            //chart.Titles.Add("Audiogram");


            var series1 = new System.Windows.Forms.DataVisualization.Charting.Series
            {
                Name = "Series1",
                Color = System.Drawing.Color.Green,
                IsVisibleInLegend = false,
                IsXValueIndexed = true,
                ChartType = SeriesChartType.Spline
            };

            this.chart.Series.Add(series1);

            chart.ChartAreas[0].AxisY.Maximum = 120;
            chart.ChartAreas[0].AxisY.Interval = 10;
            
            //chart.ChartAreas[0].AxisX.Minimum = 100;
            //chart.ChartAreas[0].AxisX.Maximum = 8500;
            //chart.ChartAreas[0].AxisX.Interval = 1000;
            ushort[] freqtab = freqlist.ToArray();
            double[] voltab = vollist.ToArray();
            for (ushort i = 0; i < freqtab.Length; i++)
            {
                series1.Points.AddXY(freqtab[i], voltab[i]);
            }
            chart.Invalidate();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            sn.PlaySound();
            Thread.Sleep(3010);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Sound s = new Sound(125, 5000, 6000);
            s.PlaySound();
        }
    }
}

